package org.graphhopper.main;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

public class CreateOSMGraphCH {
	
	public static void main(String[] args) {
		
		String osmFile = Config.getProperty("monaco.osm.file");
		String graphDir = Config.getProperty("monaco.graph.ch.dir");
		
		OSMToGraphHopperReader.createGraph(osmFile, graphDir, true, false);
	}
}
