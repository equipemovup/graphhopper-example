package org.graphhopper.main;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

public class CreateOSMGraphMobile {
	
	public static void main(String[] args) {
		
		String osmFile = Config.getProperty("berlin.osm.file");
		String graphDir = Config.getProperty("berlin.graph.dir.mobile");
		
		OSMToGraphHopperReader.createGraph(osmFile, graphDir, false, true);
	}
}
