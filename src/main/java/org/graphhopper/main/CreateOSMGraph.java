package org.graphhopper.main;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

import com.graphhopper.GraphHopper;

public class CreateOSMGraph {
	
	public static void main(String[] args) {
		
		String osmFile = Config.getProperty("berlin.osm.file");
		String graphDir = Config.getProperty("berlin.graph.dir");
		
		GraphHopper hopper = OSMToGraphHopperReader.createGraph(osmFile, graphDir, false, false);
		System.out.println(hopper);

	}
}
