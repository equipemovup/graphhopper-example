package org.graphhopper.config;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {

	private static Logger logger = LoggerFactory.getLogger(Config.class);
	private static Properties config;
	
	public static final String CONFIG_FILE = "config.properties";
	public static final String USER_HOME = System.getProperty("user.home");
	
	static{
		reload();
	}
	
	public static void reload(){
		config = new Properties();
		try {
			config.load(Config.class.getResourceAsStream("/" + CONFIG_FILE));
			Enumeration<String> props = (Enumeration<String>) config.propertyNames();
			while(props.hasMoreElements()){
				String s = props.nextElement();
				config.setProperty(s, config.getProperty(s).replace("${user.home}", USER_HOME));
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public static Properties getProperties() {
		return config;
	}
	
	public static String getProperty(String key){
		return config.getProperty(key);
	}
	
}