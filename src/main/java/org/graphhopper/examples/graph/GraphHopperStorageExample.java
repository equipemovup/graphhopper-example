package org.graphhopper.examples.graph;

import java.io.ObjectInputStream.GetField;
import org.graphhopper.config.Config;

import com.graphhopper.routing.Dijkstra;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.EdgeIterator;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.Helper;

public class GraphHopperStorageExample {
	
	public static void main(String[] args) {
		String defaultGraphLoc = Config.getProperty("example.graph.dir");
	    
	    EncodingManager encodingManager = new EncodingManager("car");
	    GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
	    GraphStorage graphStorage = gb.create();
		
		NodeAccess na = graphStorage.getNodeAccess();
        na.setNode(0, 0, 2);
        na.setNode(1, 0, 1);
        na.setNode(2, 0, 4);
        na.setNode(3, 0, 3);
        na.setNode(4, 0, 5);
        na.setNode(5, 1, 1);

        EdgeIteratorState iter1 = graphStorage.edge(1, 5, 2, true);
        iter1.setWayGeometry(Helper.createPointList(3.5, 4.5, 5, 6));
        EdgeIteratorState iter2 = graphStorage.edge(5, 0, 3, false);
        iter2.setWayGeometry(Helper.createPointList(1.5, 1, 2, 3));
        graphStorage.edge(0, 3, 2, true);
        graphStorage.edge(3, 2, 2, false);
        graphStorage.edge(2, 3, 2, false);
        graphStorage.edge(2, 4, 2, true);
        
        iter1.setName("named street1");
        iter2.setName("named street2");

        
        graphStorage.flush();
        graphStorage.close();
        
        graphStorage = gb.load();

        EdgeIterator edgeIterator = graphStorage.getAllEdges();
        
        
        
        
        while(edgeIterator.next()) {
        	System.out.println("edgeIteratorId: " + edgeIterator.getEdge());
			System.out.println("edgeIteratorFrom: " + edgeIterator.getBaseNode());
			System.out.println("edgeIteratorTo: " + edgeIterator.getAdjNode());
			System.out.println("getFlags: " + (edgeIterator.getFlags() & 3));
			System.out.println("Direction: " + getDirection(edgeIterator.getFlags()));
			System.out.println("\n");
        }
        
        
        
        System.out.println("Number of Nodes: " + graphStorage.getNodes());
        
        LocationIndex index = new LocationIndexTree(graphStorage, new RAMDirectory(defaultGraphLoc, true));
        if (!index.loadExisting()){
        	index.prepareIndex();
        }
        
        
        Dijkstra dij = new Dijkstra(graphStorage, encodingManager.getEncoder("car"), new ShortestWeighting());
        Path path = dij.calcPath(2, 5);
        System.out.println(path);
        
        QueryResult fromQR = index.findClosest(0, 0, EdgeFilter.ALL_EDGES);
        QueryResult toQR = index.findClosest(0, 2, EdgeFilter.ALL_EDGES);
        dij = new Dijkstra(graphStorage, encodingManager.getEncoder("car"), new ShortestWeighting());
        path = dij.calcPath(fromQR, toQR);
        System.out.println(path);
        
        index.close();
	}
	
	public static String getDirection(long flags){
		
		long direction = (flags & 3);
		
		if(direction ==  1) {

			return "Forward"; 
			
		} else if(direction ==  2) {
			
			return "Backward";
		} else {
			return "Bidirectional";
		}
		
	}
	
}