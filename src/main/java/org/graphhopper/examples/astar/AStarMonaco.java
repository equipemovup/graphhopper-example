package org.graphhopper.examples.astar;

import org.graphhopper.config.Config;

import com.graphhopper.routing.AStar;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.StopWatch;

public class AStarMonaco {
	public static void main(String[] args) {
		String defaultGraphLoc = Config.getProperty("monaco.graph.dir");
	    
	    EncodingManager encodingManager = new EncodingManager("car");
	    GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
	    GraphStorage graphStorage = gb.create();
	    
		graphStorage = gb.load();
        LocationIndex index = new LocationIndexTree(graphStorage, new RAMDirectory(defaultGraphLoc, true));
        if (!index.loadExisting()){
        	index.prepareIndex();
        }
        
        //sao paulo
//        QueryResult fromQR = index.findClosest(-23.520613, -46.648568, EdgeFilter.ALL_EDGES);
//        QueryResult toQR = index.findClosest(-23.741764, -46.524188, EdgeFilter.ALL_EDGES);
        
        //porto alegre a sao paulo
        QueryResult fromQR = index.findClosest(43.72842465479131, 7.414896579419745, EdgeFilter.ALL_EDGES);
        QueryResult toQR = index.findClosest(43.7354373276704, 7.4212202598427295, EdgeFilter.ALL_EDGES);
        
        StopWatch sw = new StopWatch();
        sw.start();
        AStar dij = new AStar(graphStorage, encodingManager.getEncoder("car"), new ShortestWeighting());
        Path path = dij.calcPath(fromQR, toQR);
        sw.stop();
        System.out.println(path);
        System.out.println("time:" + sw.getTime());
        
        index.close();
	}
}
