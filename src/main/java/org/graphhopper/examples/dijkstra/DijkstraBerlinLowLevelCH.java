package org.graphhopper.examples.dijkstra;

import org.graphhopper.config.Config;

import com.graphhopper.routing.Path;
import com.graphhopper.routing.RoutingAlgorithm;
import com.graphhopper.routing.ch.PrepareContractionHierarchies;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.LevelGraphStorage;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndexTreeSC;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.StopWatch;

public class DijkstraBerlinLowLevelCH {
	public static void main(String[] args) {
		String defaultGraphLoc = Config.getProperty("monaco.graph.ch.dir");
	    
	    EncodingManager encodingManager = new EncodingManager("car");
	    GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
	    LevelGraphStorage graphStorage = gb.levelGraphCreate();
	    
	    graphStorage = (LevelGraphStorage) gb.load();
	    LocationIndexTreeSC index = new LocationIndexTreeSC(graphStorage, new RAMDirectory(defaultGraphLoc, true));
	    if (!index.loadExisting()){
        	index.prepareIndex();
        }
        
        //oeste leste
        QueryResult fromQR = index.findClosest(43.728425,7.414897, EdgeFilter.ALL_EDGES);
        System.out.println("from:" + fromQR.getQueryPoint().lat + "," + fromQR.getQueryPoint().lon);
        QueryResult toQR = index.findClosest(43.737118,7.416867, EdgeFilter.ALL_EDGES);
        System.out.println("to:" + toQR.getQueryPoint().lat + "," + toQR.getQueryPoint().lon);
        
        StopWatch sw = new StopWatch();
        sw.start();
        PrepareContractionHierarchies pch = new PrepareContractionHierarchies(encodingManager.getEncoder("car"), new ShortestWeighting());
        pch.setGraph(graphStorage);
        RoutingAlgorithm algorithm = pch.createAlgo();
        Path path = algorithm.calcPath(fromQR, toQR);
        System.out.println(path);
        System.out.println("time cost:" + path.getMillis());
        System.out.println("execution time:" + sw.getTime());
        
        index.close();
	}
}
