package org.graphhopper.examples.dijkstra;

import java.util.HashSet;
import java.util.Set;

import org.graphhopper.config.Config;

import com.graphhopper.routing.Path;
import com.graphhopper.routing.RoutingAlgorithm;
import com.graphhopper.routing.ch.PrepareContractionHierarchies;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.LevelGraphStorage;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.EdgeIterator;

public class graphhopperExample4CH {
	public static void main(String[] args) {
		String defaultGraphLoc = Config.getProperty("example.graph.dir");

		EncodingManager encodingManager = new EncodingManager("car");
		GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
		LevelGraphStorage graphStorage = gb.levelGraphCreate();

		NodeAccess na = graphStorage.getNodeAccess();
		na.setNode(0, 10, 30);
		na.setNode(1, 10, 10);
		na.setNode(2, 20, 10);
		na.setNode(3, 30, 10);
		na.setNode(4, 40, 10);
		na.setNode(5, 50, 0);
		na.setNode(6, 60, 0);
		na.setNode(7, 70, 10);
		na.setNode(8, 80, 10);
		na.setNode(9, 20, 30);
		na.setNode(10, 30, 30);
		na.setNode(11, 40, 30);
		na.setNode(12, 50, 20);
		na.setNode(13, 60, 20);
		na.setNode(14, 10, 0);
		na.setNode(15, -10, 10);
		na.setNode(16, -10, 30);
		na.setNode(17, 10, 20);
		na.setNode(18, 10, -10);
		na.setNode(19, 10, -20);
		na.setNode(20, 0, -10);
		na.setNode(21, 0, -20);
		na.setNode(22, 80, 0);
		na.setNode(23, 80, -10);
		na.setNode(24, 70, -10);
		na.setNode(25, 50, -10);
		na.setNode(26, 40, -30);
		na.setNode(27, 40, -10);
		na.setNode(28, 40, 50);
		na.setNode(29, 50, 50);
		na.setNode(30, 50, 40);
		na.setNode(31, 50, 30);

		graphStorage.edge(16, 0, 1, false);
		graphStorage.edge(0, 16, 1, false);
		graphStorage.edge(0, 9, 1, false);
		graphStorage.edge(9, 0, 1, false);
		graphStorage.edge(0, 17, 1, false);
		graphStorage.edge(17, 0, 1, false);
		graphStorage.edge(9, 10, 1, false);
		graphStorage.edge(10, 9, 1, false);
		graphStorage.edge(10, 11, 1, false);
		graphStorage.edge(11, 10, 1, false);
		graphStorage.edge(11, 28, 1, false);
		graphStorage.edge(28, 11, 1, false);
		graphStorage.edge(28, 29, 1, false);
		graphStorage.edge(29, 28, 1, false);
		graphStorage.edge(29, 30, 1, false);
		graphStorage.edge(30, 29, 1, false);
		graphStorage.edge(30, 31, 1, false);
		graphStorage.edge(31, 30, 1, false);
		graphStorage.edge(31, 4, 1, false);
		graphStorage.edge(4, 31, 1, false);
		graphStorage.edge(17, 1, 1, false);
		graphStorage.edge(1, 17, 1, false);
		graphStorage.edge(15, 1, 1, false);
		graphStorage.edge(1, 15, 1, false);
		graphStorage.edge(14, 1, 1, false);
		graphStorage.edge(1, 14, 1, false);
		graphStorage.edge(14, 18, 1, false);
		graphStorage.edge(18, 14, 1, false);
		graphStorage.edge(18, 19, 1, false);
		graphStorage.edge(19, 18, 1, false);
		graphStorage.edge(19, 20, 1, false);
		graphStorage.edge(20, 19, 1, false);
		graphStorage.edge(19, 21, 1, false);
		graphStorage.edge(21, 19, 1, false);
		graphStorage.edge(21, 16, 1, false);
		graphStorage.edge(16, 21, 1, false);
		graphStorage.edge(1, 2, 1, false);
		graphStorage.edge(2, 1, 1, false);
		graphStorage.edge(2, 3, 1, false);
		graphStorage.edge(3, 2, 1, false);
		graphStorage.edge(3, 4, 1, false);
		graphStorage.edge(4, 3, 1, false);
		graphStorage.edge(4, 5, 1, false);
		graphStorage.edge(5, 6, 1, false);
		graphStorage.edge(6, 7, 1, false);
		graphStorage.edge(7, 13, 1, false);
		graphStorage.edge(13, 12, 1, false);
		graphStorage.edge(12, 4, 1, false);
		graphStorage.edge(7, 8, 1, false);
		graphStorage.edge(8, 7, 1, false);
		graphStorage.edge(8, 22, 1, false);
		graphStorage.edge(22, 8, 1, false);
		graphStorage.edge(22, 23, 1, false);
		graphStorage.edge(23, 22, 1, false);
		graphStorage.edge(23, 24, 1, false);
		graphStorage.edge(24, 23, 1, false);
		graphStorage.edge(24, 25, 1, false);
		graphStorage.edge(25, 24, 1, false);
		graphStorage.edge(25, 27, 1, false);
		graphStorage.edge(27, 25, 1, false);
		graphStorage.edge(27, 5, 1, false);
		graphStorage.edge(5, 27, 1, false);
		graphStorage.edge(25, 26, 1, false);
		graphStorage.edge(26, 25, 1, false);

		graphStorage.flush();
		graphStorage.close();

		graphStorage = (LevelGraphStorage) gb.load();

		EdgeIterator edgeIterator = graphStorage.getAllEdges();

		while (edgeIterator.next()) {
			System.out.println("edgeIteratorId: " + edgeIterator.getEdge());
			System.out.println("edgeIteratorFrom: " + edgeIterator.getBaseNode());
			System.out.println("edgeIteratorTo: " + edgeIterator.getAdjNode());
			System.out.println("getFlags: " + (edgeIterator.getFlags() & 3));
			System.out.println("Direction: " + getDirection(edgeIterator.getFlags()));
			System.out.println("\n");
		}

		System.out.println("Number of Nodes: " + graphStorage.getNodes());

		LocationIndex index = new LocationIndexTree(graphStorage, new RAMDirectory(defaultGraphLoc, true));
		if (!index.loadExisting()) {
			index.prepareIndex();
		}

		QueryResult fromQR = index.findClosest(10, 10, EdgeFilter.ALL_EDGES);
		QueryResult toQR = index.findClosest(10, -20, EdgeFilter.ALL_EDGES);
		PrepareContractionHierarchies pch = new PrepareContractionHierarchies(encodingManager.getEncoder("car"),
				new ShortestWeighting());
		pch.setGraph(graphStorage);
		pch.doWork();
		RoutingAlgorithm algorithm = pch.createAlgo();
		Path path = algorithm.calcPath(fromQR, toQR);
		System.out.println(path);

		edgeIterator = graphStorage.getAllEdges();
		Set<Integer> visitedNodes = new HashSet<Integer>();
		
//		if(visitedNodes.contains(edgeIterator.getBaseNode())) {
//			
//		} else {
//			System.out.println("Node: " + edgeIterator.getBaseNode() + ", Level: " + graphStorage.getLevel(edgeIterator.getBaseNode()));
//			visitedNodes.add(edgeIterator.getBaseNode());
//		}
//		
//		if(visitedNodes.contains(edgeIterator.getAdjNode())) {
//			
//		} else {
//			System.out.println("Node: " + edgeIterator.getAdjNode() + ", Level: " + graphStorage.getLevel(edgeIterator.getAdjNode()));
//			visitedNodes.add(edgeIterator.getAdjNode());
//		}
//		
//		while (edgeIterator.next()) {
//			if(visitedNodes.contains(edgeIterator.getBaseNode())) {
//				continue;
//			} else {
//				System.out.println("Node: " + edgeIterator.getBaseNode() + ", Level: " + graphStorage.getLevel(edgeIterator.getBaseNode()));
//				visitedNodes.add(edgeIterator.getBaseNode());
//			}
//			
//			if(visitedNodes.contains(edgeIterator.getAdjNode())) {
//				continue;
//			} else {
//				System.out.println("Node: " + edgeIterator.getAdjNode() + ", Level: " + graphStorage.getLevel(edgeIterator.getAdjNode()));
//				visitedNodes.add(edgeIterator.getAdjNode());
//			}
//			
//			
//
//		}

		index.close();
	}

	public static String getDirection(long flags) {

		long direction = (flags & 3);

		if (direction == 1) {

			return "Forward";

		} else if (direction == 2) {

			return "Backward";
		} else {
			return "Bidirectional";
		}

	}
}
