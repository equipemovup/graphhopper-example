package org.graphhopper.examples.dijkstra;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.util.StopWatch;

public class DijkstraBerlinHighLevelMobile {

	public static void main(String[] args) {
		String osmFile = Config.getProperty("berlin.osm.file");
		String graphDir = Config.getProperty("berlin.graph.dir.mobile");
		
		GraphHopper hopper = OSMToGraphHopperReader.createGraph(osmFile, graphDir, false, true);

		StopWatch sw = new StopWatch();
		sw.start();
		GHRequest req = new GHRequest(52.535926,13.192974, 52.52651,13.493285).setVehicle("car").setAlgorithm("dijkstrabi");
		GHResponse res = hopper.route(req);
		sw.stop();
		System.out.println(DijkstraBerlinHighLevelMobile.class);
		System.out.println("execution time(s):" + sw.getSeconds());
		System.out.println("distance:" + res.getDistance());
		System.out.println("points:" + res.getPoints().getSize());
		System.out.println("time(min):" + res.getMillis() / 1000.0 / 60.0);
		System.out.println(res.getInstructions());
	}
}
