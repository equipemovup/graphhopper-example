package org.graphhopper.examples.dijkstra;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

import com.graphhopper.routing.DijkstraBidirection;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.StopWatch;

public class DijkstraExampleLowLevel {
	
	public static void main(String[] args) {
		
		String defaultGraphLoc = "/tmp/graphhopper/test/example";
		//String defaultGraphLoc = Config.getProperty("example.graph.dir");
//		String osmFile = Config.getProperty("example.osm.file");
	
		String osmFile = null;

		OSMToGraphHopperReader.createGraph(osmFile, defaultGraphLoc, false, false);
	    
		EncodingManager encodingManager = new EncodingManager("car");
	    GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
	    GraphStorage graphStorage = gb.create();
	    
		graphStorage = gb.load();

        StopWatch sw = new StopWatch();
        sw.start();
        DijkstraBidirection dij = new DijkstraBidirection(graphStorage, encodingManager.getEncoder("car"), new ShortestWeighting());
        Path path = dij.calcPath(1, 4);
        sw.stop();
        System.out.println(path);
        System.out.println("time cost:" + path.getMillis());
        System.out.println("execution time:" + sw.getTime());
        
        for(Instruction il : path.calcInstructions(null)) {
        	System.out.println(il);
        }
	
	}
}
