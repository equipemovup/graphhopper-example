package org.graphhopper.examples.dijkstra;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

import com.graphhopper.routing.AStar;
import com.graphhopper.routing.AStarBidirection;
import com.graphhopper.routing.DijkstraBidirection;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.StopWatch;

public class DijkstraMonacoLowLevel {
	public static void main(String[] args) {
		String defaultGraphLoc = Config.getProperty("monaco.graph.dir");
		String osmFile = Config.getProperty("monaco.osm.file");
		
		OSMToGraphHopperReader.createGraph(osmFile, defaultGraphLoc, false, false);
	    
		EncodingManager encodingManager = new EncodingManager("car");
	    GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
	    GraphStorage graphStorage = gb.create();
	    
		graphStorage = gb.load();
        LocationIndex index = new LocationIndexTree(graphStorage, new RAMDirectory(defaultGraphLoc, true));
        if (!index.loadExisting()){
        	index.prepareIndex();
        }
        
        //oeste leste
        double originalFromLat = 43.728425;
        double originalFromLon = 7.414897;
        double originalToLat = 43.727429;
        double originalToLon = 7.422287;
        
        QueryResult fromQR = index.findClosest(originalFromLat, originalFromLon, EdgeFilter.ALL_EDGES);
        System.out.println("fromQR: " + fromQR);
        int closestNode = fromQR.getClosestNode();
        System.out.println("fromLat:" + graphStorage.getNodeAccess().getLat(closestNode));
        System.out.println("fromLong:" + graphStorage.getNodeAccess().getLon(closestNode));
        QueryResult toQR = index.findClosest(originalToLat, originalToLon, EdgeFilter.ALL_EDGES);
        System.out.println("toQR: " + toQR);
        closestNode = toQR.getClosestNode();
        System.out.println("toLat:" + graphStorage.getNodeAccess().getLat(closestNode));
        System.out.println("toLong:" + graphStorage.getNodeAccess().getLon(closestNode));
        
        StopWatch sw = new StopWatch();
        sw.start();
        AStarBidirection dij = new AStarBidirection(graphStorage, encodingManager.getEncoder("car"), new ShortestWeighting());
        Path path = dij.calcPath(fromQR, toQR);
        System.out.println("#VERTICES VISITADOS: " + dij.getVisitedNodes());
        sw.stop();
        System.out.println(path);
        System.out.println("time cost:" + path.getMillis());
        System.out.println("execution time:" + sw.getTime());
        for(Instruction il : path.calcInstructions(null)) {
        	System.out.println(il);
        }
        
        index.close();
	}
}
