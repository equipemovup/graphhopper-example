package org.graphhopper.examples.dijkstra;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

import com.graphhopper.routing.DijkstraBidirection;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.ShortestWeighting;
import com.graphhopper.storage.GraphBuilder;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.RAMDirectory;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.StopWatch;

public class DijkstraTokyoBiggerLowLevel {
	public static void main(String[] args) {
		String defaultGraphLoc = Config.getProperty("tokyoBigger.graph.dir");
		String osmFile = Config.getProperty("tokyoBigger.osm.file");
		
		OSMToGraphHopperReader.createGraph(osmFile, defaultGraphLoc, false, false);
	    
		EncodingManager encodingManager = new EncodingManager("car");
	    GraphBuilder gb = new GraphBuilder(encodingManager).setLocation(defaultGraphLoc).setStore(true);
	    GraphStorage graphStorage = gb.create();
	    
		graphStorage = gb.load();
        LocationIndex index = new LocationIndexTree(graphStorage, new RAMDirectory(defaultGraphLoc, true));
        if (!index.loadExisting()){
        	index.prepareIndex();
        }
        
        //oeste leste
        double originalFromLat = 35.70976504779827;
        double originalFromLon = 139.62002671405733;
        double originalToLat = 35.77867435169237;
        double originalToLon = 139.62009637698748;
        
        QueryResult fromQR = index.findClosest(originalFromLat, originalFromLon, EdgeFilter.ALL_EDGES);
        System.out.println("fromQR: " + fromQR);
        int closestNode = fromQR.getClosestNode();
        System.out.println("fromLat:" + graphStorage.getNodeAccess().getLat(closestNode));
        System.out.println("fromLong:" + graphStorage.getNodeAccess().getLon(closestNode));
        QueryResult toQR = index.findClosest(originalToLat, originalToLon, EdgeFilter.ALL_EDGES);
        System.out.println("toQR: " + toQR);
        closestNode = toQR.getClosestNode();
        System.out.println("toLat:" + graphStorage.getNodeAccess().getLat(closestNode));
        System.out.println("toLong:" + graphStorage.getNodeAccess().getLon(closestNode));
        
        StopWatch sw = new StopWatch();
        sw.start();
        DijkstraBidirection dij = new DijkstraBidirection(graphStorage, encodingManager.getEncoder("car"), new ShortestWeighting());
        Path path = dij.calcPath(fromQR, toQR);
        sw.stop();
        System.out.println(path);
        System.out.println("time cost:" + path.getMillis());
        System.out.println("execution time:" + sw.getTime());
        for(Instruction il : path.calcInstructions(null)) {
        	System.out.println(il);
        }
        
        index.close();
	}
}
