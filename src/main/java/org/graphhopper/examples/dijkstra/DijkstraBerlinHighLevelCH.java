package org.graphhopper.examples.dijkstra;

import org.graphhopper.config.Config;
import org.graphhopper.reader.OSMToGraphHopperReader;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.util.StopWatch;

public class DijkstraBerlinHighLevelCH {

	public static void main(String[] args) {
		String osmFile = Config.getProperty("monaco.osm.file");
		String graphDir = Config.getProperty("monaco.graph.dir");
		
		GraphHopper hopper = OSMToGraphHopperReader.createGraph(osmFile, graphDir, true, false);

		StopWatch sw = new StopWatch();
		sw.start();
		GHRequest req = new GHRequest(43.728425,7.414897, 43.727429,7.422287).setVehicle("car").setAlgorithm("dijkstrabi");
		GHResponse res = hopper.route(req);
		sw.stop();
		System.out.println(DijkstraBerlinHighLevelCH.class);
		System.out.println("execution time(s):" + sw.getSeconds());
		System.out.println("distance:" + res.getDistance());
		System.out.println("points:" + res.getPoints().getSize());
		System.out.println("time(min):" + res.getMillis() / 1000.0 / 60.0);
		System.out.println(res.getInstructions());
	}
}
